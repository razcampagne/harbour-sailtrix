#include "roomdirectorymodel.h"

QHash<int, QByteArray> RoomDirectoryModel::roleNames() const {
    QHash<int, QByteArray> list;
    list[room_id] = "room_id";
    list[name] = "name";
    list[alias] = "alias";
    list[joined_count] = "joined_count";
    list[avatar_url] = "avatar_url";
    list[avatar_path] = "avatar_path";
    list[description] = "description";
    return list;
}
