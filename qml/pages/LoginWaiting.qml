import QtQuick 2.0
import Sailfish.Silica 1.0
import LoginBridge 1.0

Page {
    id: page
    allowedOrientations: Orientation.All

    property LoginBridge lBridge

    Column {

        width: page.width
        anchors.centerIn: parent

        BusyIndicator {
            size: BusyIndicatorSize.Large
            running: !lBridge.error
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            id: label
            text: lBridge.error
            color: Theme.errorColor
            wrapMode: Text.Wrap
            width: parent.width
            anchors.left: parent.left
            anchors.right: parent.right

            onTextChanged: {
                if (text == "Done") {
                    pageStack.clear();
                    pageStack.replace("Rooms.qml")
                }
            }
        }
    }
}
