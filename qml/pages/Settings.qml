import QtQuick 2.0
import Sailfish.Silica 1.0
import SettingsBackend 1.0

Page {
    id: page

    allowedOrientations: Orientation.All

    property bool running;

    SettingsBackend {
        id: backend;
        onDone: {
            pageStack.replaceAbove(null, "Rooms.qml");
        }
        onConfigClearDone: {
            pageStack.replaceAbove(null, "Start.qml");
        }
        onNotificationDisabledChanged: {
            if (notificationDisabled) {
                console.log("Disabling notifs")
                sailtrixSignals.emitNotificationsDisabled();
            } else {
                sailtrixSignals.emitNotificationsEnabled();
            }
        }

        onNotifIntervalChanged: {
            sailtrixSignals.emitNotificationIntervalChanged(notifInterval);
        }

        onBackgroundServiceDisabledChanged: {
            if (backgroundServiceDisabled) {
                sailtrixSignals.emitBackgroundServiceDisabled();
            } else {
                sailtrixSignals.emitBackgroundServiceEnabled()
            }
        }
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            spacing: Theme.paddingLarge
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.rightMargin: Theme.horizontalPageMargin
            anchors.left: parent.left
            anchors.right: parent.right

            PageHeader {
                title: qsTr("Settings")
            }

            SectionHeader {
                text: qsTr("Notifications")
            }

            TextSwitch {
                id: notif_enable
                text: qsTr("Enable notifications")
                checked: !backend.notificationDisabled
                onCheckedChanged: backend.notificationDisabled = !checked
            }

            TextSwitch {
                id: service_enable
                text: qsTr("Enable notifications when app is closed")
                visible: notif_enable.checked
                checked: !backend.backgroundServiceDisabled
                onCheckedChanged: backend.backgroundServiceDisabled = !checked
            }

            ComboBox {
                visible: notif_enable.checked
                label: qsTr("Notification checking interval")
                menu: ContextMenu {
                    MenuItem { text: qsTr("30 seconds") }
                    MenuItem { text: qsTr("2.5 minutes") }
                    MenuItem { text: qsTr("5 minutes") }
                    MenuItem { text: qsTr("10 minutes") }
                    MenuItem { text: qsTr("15 minutes") }
                    MenuItem { text: qsTr("30 minutes") }
                    MenuItem { text: qsTr("1 hour") }
                    MenuItem { text: qsTr("2 hours") }
                    MenuItem { text: qsTr("4 hours") }
                    MenuItem { text: qsTr("8 hours") }
                    MenuItem { text: qsTr("10 hours") }
                    MenuItem { text: qsTr("12 hours") }
                    MenuItem { text: qsTr("24 hours") }
                }

                currentIndex: backend.notifInterval
                onCurrentIndexChanged: {
                    backend.notifInterval = currentIndex
                }
            }

            SectionHeader {
                text: qsTr("Display")
            }

            ComboBox {
                id: sort_type
                label: qsTr("Sort rooms by")
                menu: ContextMenu {
                    MenuItem { text: qsTr("Activity") }
                    MenuItem { text: qsTr("Alphabetical") }
                }

                currentIndex: backend.sortType
                onCurrentIndexChanged: backend.sortType = currentIndex
            }

            SectionHeader {
                text: qsTr("Global")
            }

            Button {
                text: qsTr("Clear cache")
                onClicked: Remorse.popupAction(root, qsTr("Cleared cache"), function() { backend.clear_cache() });
            }

            Button {
                text: qsTr("Logout")
                onClicked: {
                    Remorse.popupAction(root, qsTr("Logging out"), function() {
                        running = true;
                        backend.clear_config();
                    });
                }
            }


            VerticalScrollDecorator{}
        }


    }
    PageBusyIndicator {
        id: indicator
        running: running
    }
}
