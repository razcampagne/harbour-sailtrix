import QtQuick 2.0
import Sailfish.Silica 1.0
import DBusActivation 1.0
import SailtrixSignals 1.0
import "pages"
import "sf-docked-tab-bar"

ApplicationWindow
{
    initialPage: Component { Start { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    property alias tabBar: tabbar
    property bool has_invites: false;

    DockedTabBar {
        id: tabbar
        currentSelection: 0
        enabledOnPage: "Rooms"

        DockedTabButton {
            icon.source: "image://theme/icon-m-chat"
            label: "Rooms"
            fontSize: Theme.fontSizeTiny
        }
        DockedTabButton {
            icon.source: "image://theme/icon-m-people"
            label: "People"
            fontSize: Theme.fontSizeTiny
        }

        DockedTabButton {
            icon.source: "image://theme/icon-m-mail-open"
            label: "Invites"
            fontSize: Theme.fontSizeTiny
        }
    }
 }
