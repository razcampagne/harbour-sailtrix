<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>CreateRoom</name>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="50"/>
        <source>Create Room</source>
        <translation>Utwórz pokój</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="51"/>
        <source>Create</source>
        <translation>Utwórz</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="56"/>
        <source>Visibility</source>
        <translation>Widoczność</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="58"/>
        <source>Private</source>
        <translation>Prywatny</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="59"/>
        <source>Public</source>
        <translation>Publiczny</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="70"/>
        <location filename="../qml/pages/CreateRoom.qml" line="71"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="79"/>
        <source>Topic</source>
        <translation>Temat</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="80"/>
        <source>Topic (optional)</source>
        <translation>Temat (opcjonalnie)</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="101"/>
        <location filename="../qml/pages/CreateRoom.qml" line="102"/>
        <source>Room alias</source>
        <translation>Alias pokoju</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="119"/>
        <source>Enable end-to-end encryption</source>
        <translation>Włącz szyfrowanie end-to-end</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="120"/>
        <source>Bridges and most bots won&apos;t work yet.</source>
        <translation>Mosty i większość botów jeszcze nie działa</translation>
    </message>
</context>
<context>
    <name>Credits</name>
    <message>
        <location filename="../qml/pages/Credits.qml" line="10"/>
        <source>Credits</source>
        <translation>Podziękowania</translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <location filename="../qml/pages/Invite.qml" line="15"/>
        <source>Invite</source>
        <translation>zaproś</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source>You&apos;ve been invited to</source>
        <translation>Zostałeś zaproszony do</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source> chat with</source>
        <translation>czat z</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="51"/>
        <source>Join</source>
        <translation>Dołącz</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="65"/>
        <source>Reject</source>
        <translation>Odrzuć</translation>
    </message>
</context>
<context>
    <name>JoinPublicRoom</name>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="19"/>
        <source>Join Room</source>
        <translation>Dołącz do pokoju</translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="20"/>
        <source>Join</source>
        <translation>Dołącz</translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="78"/>
        <source> members</source>
        <translation> członkowie</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="28"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="32"/>
        <source>Homeserver</source>
        <translation>Serwer domowy</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="33"/>
        <source>Homeserver URL (matrix.org)</source>
        <translation>URL serwera domowego (matrix.org)</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="41"/>
        <source>Username</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="42"/>
        <source>Username (user1)</source>
        <translation>Nazwa użytkownika (user1)</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="50"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../qml/pages/Messages.qml" line="36"/>
        <location filename="../qml/pages/Messages.qml" line="39"/>
        <source>File saved</source>
        <translation>Plik zapisany</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="37"/>
        <source>File saved to Downloads directory</source>
        <translation>Plik zapisany do katalogu Pobrane</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="125"/>
        <source>Copy</source>
        <translation>Kopiuj</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="131"/>
        <source>Reply</source>
        <translation>Odpowiedz</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="134"/>
        <source>Replying to </source>
        <translation>Odpowiadanie do</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="147"/>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="165"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="255"/>
        <source>Download audio</source>
        <translation>Pobierz dźwięk</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="287"/>
        <location filename="../qml/pages/Messages.qml" line="307"/>
        <source>View video</source>
        <translation>Zobacz film</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="287"/>
        <source>Download</source>
        <translation>Pobierz</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="316"/>
        <source>Download file</source>
        <translation>Pobierz plik</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="408"/>
        <source>Message</source>
        <translation>Wiadomość</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="409"/>
        <source>Send message to room</source>
        <translation>Wyślij wiadomość do pokoju</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="452"/>
        <source>New Messages</source>
        <translation>Nowa wiadomość</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="473"/>
        <source>Send file</source>
        <translation>Wyślij plik</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1288"/>
        <source> invited </source>
        <translation> zaproszony</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1292"/>
        <source> changed their profile</source>
        <translation> zmieniono ich profil</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1294"/>
        <source> joined</source>
        <translation> dołączył</translation>
    </message>
</context>
<context>
    <name>PictureDisplay</name>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="17"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="20"/>
        <source>Image saved</source>
        <translation>Obrazek zapisany</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="18"/>
        <source>Image saved to Pictures directory</source>
        <translation>Obrazek zapisany do katalogu Obrazy</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="26"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="29"/>
        <source>Image error</source>
        <translation>Błąd obrazka</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="27"/>
        <source>Image not saved to Pictures directory</source>
        <translation>Obrazek nie został zapisany do katalogu Obrazy</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="51"/>
        <source>Save to Pictures</source>
        <translation>Zapisz do Obrazy</translation>
    </message>
</context>
<context>
    <name>RoomDirectory</name>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="28"/>
        <source>Explore public rooms</source>
        <translation>Eksploruj publiczne pokoje</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="33"/>
        <source>Find a room...</source>
        <translation>Znajdź pokój...</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="46"/>
        <source>Search in</source>
        <translation>Szukaj w</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="49"/>
        <source>My homeserver</source>
        <translation>Mój serwer domowy</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="50"/>
        <source>Matrix.org</source>
        <translation>Matrix.org</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="60"/>
        <source>No results</source>
        <translation>Brak wyników</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="61"/>
        <source>Please try a different search term</source>
        <translation>Spróbuj innego zapytania</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="71"/>
        <source>Search for a room</source>
        <translation>Szukaj pokoju</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="72"/>
        <source>Use the search box to search for a room</source>
        <translation>Uzyj pola szukania aby znaleźć pokój</translation>
    </message>
</context>
<context>
    <name>RoomsDisplay</name>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="43"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="48"/>
        <source>Explore public rooms</source>
        <translation>Eksploruj publiczne pokoje</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="54"/>
        <source>Create new room</source>
        <translation>Utwórz nowy pokój</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="66"/>
        <source>No </source>
        <translation>Nie </translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="82"/>
        <source>Leave</source>
        <translation>Opusć</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="56"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="60"/>
        <source>Notifications</source>
        <translation>Powiadomienia</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="65"/>
        <source>Enable notifications</source>
        <translation>Włącz powiadomienia</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="72"/>
        <source>Enable notifications when app is closed</source>
        <translation>Włacz powiadomienia kiedy aplikacja jest zamknięta</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="80"/>
        <source>Notification checking interval</source>
        <translation>Interwał sprawdzania powiadomień</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="82"/>
        <source>30 seconds</source>
        <translation>30 sekund</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="83"/>
        <source>2.5 minutes</source>
        <translation>2,5 minuty</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="84"/>
        <source>5 minutes</source>
        <translation>5 minut</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="85"/>
        <source>10 minutes</source>
        <translation>10 minut</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="86"/>
        <source>15 minutes</source>
        <translation>15 minut</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="87"/>
        <source>30 minutes</source>
        <translation>30 minut</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>1 hour</source>
        <translation>1 godzina</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="89"/>
        <source>2 hours</source>
        <translation>2 godziny</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>4 hours</source>
        <translation>4 godziny</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="91"/>
        <source>8 hours</source>
        <translation>8 godzin</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="92"/>
        <source>10 hours</source>
        <translation>10 godzin</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="93"/>
        <source>12 hours</source>
        <translation>12 godzin</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="94"/>
        <source>24 hours</source>
        <translation>24 godziny</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="104"/>
        <source>Display</source>
        <translation>Wyświetl</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="109"/>
        <source>Sort rooms by</source>
        <translation>Sortuj pokoje po</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="111"/>
        <source>Activity</source>
        <translation>Aktywność</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Alphabetical</source>
        <translation>Alfabetycznie</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="120"/>
        <source>Global</source>
        <translation>Globalnie</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="124"/>
        <source>Clear cache</source>
        <translation>Wyczyść pamięć podręczną</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="125"/>
        <source>Cleared cache</source>
        <translation>Pamięć podręczna wyczyszczona</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="129"/>
        <source>Logout</source>
        <translation>Wyloguj</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="131"/>
        <source>Logging out</source>
        <translation>Wylogowywanie</translation>
    </message>
</context>
<context>
    <name>Start</name>
    <message>
        <location filename="../qml/pages/Start.qml" line="22"/>
        <source>About</source>
        <translation>O Sailtrix</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="40"/>
        <source>Welcome</source>
        <translation>Witaj</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="44"/>
        <source>Log in</source>
        <translation>Zaloguj</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../qml/pages/User.qml" line="83"/>
        <source>Direct Message</source>
        <translation>Wiadomość bezpośrednia</translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="94"/>
        <source>Ignore</source>
        <translation>Ignoruj</translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="104"/>
        <source>Unignore</source>
        <translation>Przestań ignorować</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="17"/>
        <location filename="../qml/pages/VideoPage.qml" line="20"/>
        <source>Video saved</source>
        <translation>Film zapisany</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="18"/>
        <source>Video saved to Videos directory</source>
        <translation>Film zapisanu do katalogu Filmy</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="26"/>
        <location filename="../qml/pages/VideoPage.qml" line="29"/>
        <source>Video error</source>
        <translation>Błąd filmu</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="27"/>
        <source>Video not saved to Videos directory</source>
        <translation>Film nie został zapisany do katalogu Filmy</translation>
    </message>
</context>
</TS>
